# Migration `20201023064726`

This migration has been generated at 10/23/2020, 12:17:26 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
ALTER TABLE "public"."User" ADD COLUMN "imageUrl" text   NOT NULL 
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20201021113236..20201023064726
--- datamodel.dml
+++ datamodel.dml
@@ -1,15 +1,17 @@
 generator client {
   provider = "prisma-client-js"
+  binaryTargets = ["native"]
 }
 datasource db {
   provider = "postgresql"
-  url = "***"
+  url = "***"
 }
 model User {
   id    String @id @default(uuid())
   name  String
   email String @unique
   age   Int
+  imageUrl String
 }
```


