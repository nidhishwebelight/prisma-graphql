# Migration `20201023105120`

This migration has been generated at 10/23/2020, 4:21:20 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql

```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20201023104225..20201023105120
--- datamodel.dml
+++ datamodel.dml
@@ -4,17 +4,18 @@
 }
 datasource db {
   provider = "postgresql"
-  url = "***"
+  url = "***"
 }
 model User {
-  id       String @id @default(uuid())
-  name     String
-  email    String @unique
-  age      Int
-  imageUrl String
+  id         String @id @default(uuid())
+  name       String
+  email      String @unique
+  age        Int
+  imageUrl   String
+  posts      Post[]
 }
 model Post {
   id         String @id @default(uuid())
```


