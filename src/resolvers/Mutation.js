var aws = require('aws-sdk');

const Mutation = {
    async createUser(parent, { data }, { prisma, UserModel, isMongoDBActive }, info) {
        const { name, email, age, imageUrl } = data;
        try {
            if (isMongoDBActive) {
                const user = new UserModel({
                    name,
                    email,
                    age,
                    imageUrl
                });
                await user.save();
                return user;
            }
            else {
                const createdUser = await prisma.user.create({
                    data: {
                        name,
                        email,
                        age,
                        imageUrl
                    }
                });
                return createdUser;
            }
        }
        catch (e) {
            throw new Error(e);
        }
    },
    async deleteUser(parent, { id }, { prisma, UserModel, isMongoDBActive }, info) {
        try {
            if (isMongoDBActive) {
                const deletedUser = await UserModel.findByIdAndDelete(id);
                return deletedUser;
            }
            else {
                const deletedUser = await prisma.user.delete({
                    where: { id },
                });
                return deletedUser;
            }
        }
        catch (e) {
            throw new Error(e);
        }
    },
    async updateUser(parent, { id, data }, { prisma, UserModel, isMongoDBActive }, info) {
        const { name, email, age } = data;
        try {
            if (isMongoDBActive) {
                const updatedUser = await UserModel.findByIdAndUpdate(id, {
                    name,
                    email,
                    age
                }, {
                    new: true
                });
                return updatedUser;
            }
            else {
                const updatedUser = await prisma.user.update({
                    where: {
                        id
                    },
                    data: {
                        name,
                        email,
                        age
                    }
                });
                return updatedUser;
            }
        }
        catch (e) {
            throw new Error(e);
        }
    },
    async createPost(parent, { data }, { prisma, PostModel, isMongoDBActive }, info) {
        const { authorId, title } = data;
        try {
            if (isMongoDBActive) {
                const post = new PostModel({
                    authorId,
                    title
                });
                await post.save();
                return post;
            }
            else {
                const user = await prisma.post.create({
                    data: {
                        title,
                        author: {
                            connect: { id: authorId },
                        },
                    },
                })
                return user;
            }
        }
        catch (e) {
            throw new Error(e);
        }
    },
    async getSignedUrl(parent, { data }, ctx, info) {
        const { fileName, fileType } = data;
        aws.config.update({
            region: process.env.REGION,
            accessKeyId: process.env.ACCESSKEYID,
            secretAccessKey: process.env.SECRETACCESSKEY
        });
        const S3_BUCKET = process.env.BUCKETNAME;
        const s3 = new aws.S3({ signatureVersion: 'v4' });
        const s3Params = {
            Bucket: S3_BUCKET,
            Key: fileName,
            ContentType: fileType
        };
        try {
            const generatePutUrl = await s3.getSignedUrl('putObject', s3Params);
            const generatedPutData = {
                signedRequest: generatePutUrl,
                url: `https://${S3_BUCKET}.s3.amazonaws.com/${fileName}`
            };
            return generatedPutData;
        }
        catch (e) {
            throw new Error(e);
        }
    }
};

export { Mutation as default };