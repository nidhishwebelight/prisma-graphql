# Migration `20201023065227`

This migration has been generated at 10/23/2020, 12:22:27 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
CREATE TABLE "public"."User" (
"id" text   NOT NULL ,
"name" text   NOT NULL ,
"email" text   NOT NULL ,
"age" integer   NOT NULL ,
PRIMARY KEY ("id")
)

CREATE UNIQUE INDEX "User.email_unique" ON "public"."User"("email")
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20201023064726..20201023065227
--- datamodel.dml
+++ datamodel.dml
@@ -1,17 +1,16 @@
 generator client {
-  provider = "prisma-client-js"
+  provider      = "prisma-client-js"
   binaryTargets = ["native"]
 }
 datasource db {
   provider = "postgresql"
-  url = "***"
+  url = "***"
 }
 model User {
   id    String @id @default(uuid())
   name  String
   email String @unique
   age   Int
-  imageUrl String
 }
```


