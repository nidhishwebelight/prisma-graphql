const Post = {
    async author(parent, args, { prisma, PostModel, isMongoDBActive }, info) {
        try {
            if (isMongoDBActive) {
                const post = await PostModel.findById(parent.id)
                    .populate('authorId')
                    .exec();
                const authorOfPost = post.authorId;
                return authorOfPost;
            }
            else {
                const authorOfPost = await prisma.user.findOne({
                    where: {
                        id: parent.authorId
                    }
                })
                return authorOfPost;
            }
        }
        catch (e) {
            throw new Error(e);
        }
    }
}

export { Post as default };