# Migration `20201021093248`

This migration has been generated at 10/21/2020, 3:02:48 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
ALTER TABLE "public"."User" ALTER COLUMN "age" SET NOT NULL

ALTER INDEX "public"."User.email" RENAME TO "User.email_unique"
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration ..20201021093248
--- datamodel.dml
+++ datamodel.dml
@@ -1,0 +1,15 @@
+generator client {
+  provider = "prisma-client-js"
+}
+
+datasource db {
+  provider = "postgresql"
+  url = "***"
+}
+
+model User {
+  id    String @id
+  name  String
+  email String @unique
+  age   Int
+}
```


