import { GraphQLServer } from 'graphql-yoga';

require('dotenv').config();
require("../mongoose/mongodb/mongoose");

import UserModel from "../mongoose/models/user";
import PostModel from "../mongoose/models/post";

import { PrismaClient } from '@prisma/client';

import Query from "./resolvers/Query";
import Mutation from "./resolvers/Mutation";
import User from "./resolvers/User";
import Post from "./resolvers/Post";

const prisma = new PrismaClient();

let isMongoDBActive = false;

if(process.env.MONGODB_URL){
    isMongoDBActive = true;
}
const server = new GraphQLServer({
    typeDefs: "./src/schema.graphql",
    resolvers: {
        Query,
        Mutation,
        User,
        Post
    },
    context: {
        prisma,
        UserModel,
        PostModel,
        isMongoDBActive
    }
})

server.start(()=>{
    console.log("Server is up on port:", server.options.port);
});