const mongoose = require('mongoose');

var validator = require('validator');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true,
        lowercase: true,
        validate(value) {
            if(!validator.isEmail(value)) {
                throw new Error("Email is invalid")
            }
        }
    },
    age: {
        type: Number,
        required: true
    },
    imageUrl: {
        type: String,
        required: true
    }
});

userSchema.virtual("posts", {
    ref: "Post",
    localField: "_id",
    foreignField: "authorId"
});

const User = mongoose.model("User", userSchema);
export { User as default };