const Query = {
    async users(parent, args, { prisma, UserModel, isMongoDBActive }, info) {
        try {
            if (isMongoDBActive) {
                const users = await UserModel.find();
                return users;
            }
            else {
                const users = await prisma.user.findMany({})
                return users;
            }
        }
        catch (e) {
            throw new Error(e);
        }
    },
    async post(parent, { id }, { prisma, PostModel, isMongoDBActive }, info) {
        try {
            if (isMongoDBActive) {
                const post = await PostModel.findById(id);
                return post;
            }
            else {
                const post = await prisma.post.findOne({
                    where: {
                        id
                    }
                });
                return post;
            }
        }
        catch (e) {
            throw new Error(e);
        }
    }
};

export { Query as default };  