# Migration `20201023104225`

This migration has been generated at 10/23/2020, 4:12:25 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
CREATE TABLE "public"."Post" (
"id" text   NOT NULL ,
"title" text   NOT NULL ,
"authorId" text   NOT NULL ,
PRIMARY KEY ("id")
)

ALTER TABLE "public"."Post" ADD FOREIGN KEY ("authorId")REFERENCES "public"."User"("id") ON DELETE CASCADE ON UPDATE CASCADE
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20201023065425..20201023104225
--- datamodel.dml
+++ datamodel.dml
@@ -4,14 +4,21 @@
 }
 datasource db {
   provider = "postgresql"
-  url = "***"
+  url = "***"
 }
 model User {
-  id    String @id @default(uuid())
-  name  String
-  email String @unique
-  age   Int
+  id       String @id @default(uuid())
+  name     String
+  email    String @unique
+  age      Int
   imageUrl String
 }
+
+model Post {
+  id         String @id @default(uuid())
+  title      String
+  author     User @relation(fields: [authorId], references: [id])
+  authorId   String
+}
```


