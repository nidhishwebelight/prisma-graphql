# Migration `20201023065425`

This migration has been generated at 10/23/2020, 12:24:25 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
CREATE TABLE "public"."User" (
"id" text   NOT NULL ,
"name" text   NOT NULL ,
"email" text   NOT NULL ,
"age" integer   NOT NULL ,
"imageUrl" text   NOT NULL ,
PRIMARY KEY ("id")
)

CREATE UNIQUE INDEX "User.email_unique" ON "public"."User"("email")
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20201023065227..20201023065425
--- datamodel.dml
+++ datamodel.dml
@@ -4,13 +4,14 @@
 }
 datasource db {
   provider = "postgresql"
-  url = "***"
+  url = "***"
 }
 model User {
   id    String @id @default(uuid())
   name  String
   email String @unique
   age   Int
+  imageUrl String
 }
```


