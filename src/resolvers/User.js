const User = {
    async posts(parent, args, { prisma, UserModel, isMongoDBActive }, info) {
        try {
            if (isMongoDBActive) {
                const user = await UserModel.findById(parent.id);
                await user.populate({ path: "posts" }).execPopulate();
                return user.posts;
            }
            else {
                const id = parent.id;
                const postsByUser = await prisma.user
                    .findOne({ where: { id } })
                    .posts()
                return postsByUser;
            }
        }
        catch (e) {
            throw new Error(e);
        }
    }
}

export { User as default };