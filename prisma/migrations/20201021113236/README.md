# Migration `20201021113236`

This migration has been generated at 10/21/2020, 5:02:36 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql

```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20201021093248..20201021113236
--- datamodel.dml
+++ datamodel.dml
@@ -3,13 +3,13 @@
 }
 datasource db {
   provider = "postgresql"
-  url = "***"
+  url = "***"
 }
 model User {
-  id    String @id
+  id    String @id @default(uuid())
   name  String
   email String @unique
   age   Int
 }
```


